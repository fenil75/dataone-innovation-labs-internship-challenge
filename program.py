import sys
import csv
from operator import itemgetter

class Shop:                                                        
	def __init__(self,shopNo):
		self.shopNo = shopNo 
		self.productList = []              # list containing all the products sold by a shop
		self.priceAndProducts = []         # list of all the ['price','products'] sold by a shop

	def addPriceAndProducts(self,price,products):  # function for appending to priceAndProduct list
		self.priceAndProducts.append([price,products])
		for product in products:
			if product not in self.productList:
				self.productList.append(product)

if __name__ == "__main__":
	argumentList = sys.argv
	data = open(argumentList[1], 'rb')    # reading the csv file
	reader = csv.reader(data)
	shops = {}							  # list for storing the object for each shop
	for row in reader:
		products = row[2].split(',')
		price = row[1]
		shopNo = row[0]
		if not shops.has_key(shopNo):     # if the shop object instance is not created than create one and store it in shops
			shop = Shop(shopNo)
			shop.addPriceAndProducts(price,products)  # add the [price,combo] to priceAndProducts of the object
			shops[shopNo] = shop
		else:
			shop = shops[shopNo]
			shop.addPriceAndProducts(price,products)
	totalPrices = []                      # list for storing the cost of the desired product input from different shops  
	customerProducts = argumentList[2:len(argumentList)] # getting the list of products requested 
	for shopNo in shops:
		shop = shops[shopNo]
		
		if set(customerProducts).union(set(shop.productList)) == set(shop.productList): # if the shop doesnot sell all the requested product then skip else continue
			
			curatedPriceAndProducts = []

			for priceAndProduct in shop.priceAndProducts: 		# ignoring the combos which donot have any product with the input products
				if not set(customerProducts).isdisjoint(priceAndProduct[1]):
					curatedPriceAndProducts.append(priceAndProduct)

			# Now we will generate the combinations of all the remaining combos to see which combination gives us the minimum cost.
			# storing the minimum cost in the totalprices variable then continue to process other shops.			
			list1 = []
			list2 = []
			flag = 0
			flag1 = []
			flag2 = []
			for i in range(0,len(curatedPriceAndProducts)):
				if customerProducts[0] in curatedPriceAndProducts[i][1]:
					flag1.append(False)
					tempList = []
					tempList.append(curatedPriceAndProducts[i])
					list1.append(tempList)
			
			
			for i in range(1,len(customerProducts)):
				product = customerProducts[i]
				
				if flag%2 == 0:
					list2 = []
					flag2 = []
					
					for j in range(0,len(curatedPriceAndProducts)):
						if customerProducts[i] in curatedPriceAndProducts[j][1]:
							
							for k in range(0,len(list1)):
								if not flag1[k]:
									
									if curatedPriceAndProducts[j] in list1[k]:  
										list2.append(list1[k])
										flag2.append(False)
									else:
										tempList = []
										
										tempList.append(curatedPriceAndProducts[j])
										list2.append(list1[k] + tempList)
										list2.append(list1[k])
										list3 = []
										
										for tempList1 in list1[k]+tempList:
											list3 += tempList1[1]

										if set(customerProducts).issubset(set(list3)):
											flag2.append(True)
										else:
											flag2.append(False)
										flag2.append(False)
								else:
									flag2.append(True)
									list2.append(list1[k])
					
					flag += 1
				else:
					list1 = []
					flag1 = []
					for j in range(0,len(curatedPriceAndProducts)):
						if customerProducts[i] in curatedPriceAndProducts[j][1]:
							for k in range(0,len(list2)):
								if not flag2[k]:
									if curatedPriceAndProducts[j] in list2[k]: 
										list1.append(list2[k])
										flag1.append(False)
									else:
										tempList = []
										tempList.append(curatedPriceAndProducts[j])
										list1.append(list2[k]+tempList)
										list1.append(list2[k])
										list3 = []
										for tempList1 in list2[k]+tempList:
											list3 += tempList1[1]
										if set(customerProducts).issubset(set(list3)):
											flag1.append(True)
										else:
											flag1.append(False)
										flag1.append(False)

								else:
									flag1.append(True)
									list1.append(list2[k])
					flag += 1

			# the combinations are stored in list1 or list2 depending on the flag value.
			# finding minimum cost and storing it in totalPrices list with shopNo
			if  flag%2 != 0:
				cost = []
				for i in range(0,len(list1)):
					tempCost = 0.0
					if flag2[i]:
						for j in list2[i]:
							tempCost += float(j[0])
						cost.append(tempCost)
				if cost:
					totalPrices.append([min(cost),int(shopNo)])
			elif flag%2 == 0:
				cost = []
				for i in range(0,len(list1)):
					tempCost = 0.0
					if flag1[i]:
						for j in list1[i]:
							tempCost += float(j[0])
						cost.append(tempCost)
				if cost:
					totalPrices.append([min(cost),int(shopNo)])
			
	totalPrices.sort(key=lambda x: int(x[0])) #sorting the totalPrices list
	if not totalPrices:
		print "none"
	else:
		print str(totalPrices[0][1])+', '+str(totalPrices[0][0]) # displaying the shop with the minimum cost.
	data.close()
